package cookies

import (
	"encoding/json"
	"net/http"
	"os"

	"log"

	"github.com/gorilla/mux"
	f "gopkg.in/fauna/faunadb-go.v2/faunadb"
)

// Cookie is a record of a single cookie
type Cookie struct {
	ID          string   `fauna:"id" json:"id"`
	Name        string   `fauna:"name" json:"name"`
	Price       float64  `fauna:"price" json:"price"`
	Unit        string   `fauna:"unit" json:"unit"`
	Image       string   `fauna:"image" json:"image"`
	Ingredients []string `fauna:"ingredients" json:"ingredients"`
	Flavors     []string `fauna:"flavors" json:"flavors"`
	Description string   `fauna:"description" json:"description"`
}

// Cookies is a list of Cookie
type Cookies []Cookie

// getFaunaByID takes a cookie ID and runs a Get with a Match in the
// ID index and returns the unique match for that ID.
func getFaunaByID(id string, client *f.FaunaClient) (*Cookie, error) {
	var cookie Cookie

	// use select to narrow down the mapping to just the JSON.
	resp, err := client.Query(
		f.Select("data", f.Get(
			f.MatchTerm(f.Index("ByID"), id),
		)))
	if err != nil {
		return nil, err
	}

	err = resp.Get(&cookie)
	if err != nil {
		return nil, err
	}

	return &cookie, nil

}

func GetCookieById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	client := f.NewFaunaClient(os.Getenv("FAUNA_TOKEN"))

	data, err := getFaunaByID(vars["cookieId"], client)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	jd, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jd)
}

// getFaunaAllCookies runs a Paginate over the All index and returns
// all of the available cookies.
/* func getFaunaAllCookies(client *f.FaunaClient) (Cookies, error) {
	// use select to narrow down the mapping to just the JSON.
	resp, err := client.Query(
		f.Map(
			f.Select("data", f.Paginate(
				f.Match(f.Index("All")))),
			f.Lambda("cookie", f.Select("data", f.Get(f.Var("cookie")))),
		),
	)
	if err != nil {
		return nil, err
	}

	var cookies Cookies

	err = resp.Get(&cookies)
	if err != nil {
		return nil, err
	}

	return cookies, nil
}
*/

// getFaunaMap takes a client, index name, and search argument and returns a
// slice of Cookies.
func getFaunaMap(client *f.FaunaClient, index string, search interface{}) (Cookies, error) {
	// use select to narrow down the mapping to just the JSON.
	resp, err := client.Query(
		f.Map(
			f.Select("data", f.Paginate(
				f.MatchTerm(f.Index(index), search))),
			f.Lambda("cookie", f.Select("data", f.Get(f.Var("cookie")))),
		),
	)
	if err != nil {
		return nil, err
	}

	var cookies Cookies

	err = resp.Get(&cookies)
	if err != nil {
		return nil, err
	}

	return cookies, nil
}

func GetCookies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	client := f.NewFaunaClient(os.Getenv("FAUNA_TOKEN"))

	data, err := getFaunaMap(client, "All", nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	jd, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jd)
}

func GetSiteCookies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	client := f.NewFaunaClient(os.Getenv("FAUNA_TOKEN"))

	data, err := getFaunaMap(client, "Site", true)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	jd, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jd)
}

func GetCookieByIngredients(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	client := f.NewFaunaClient(os.Getenv("FAUNA_TOKEN"))

	data, err := getFaunaMap(client, "ByIngredients", vars["ingredient"])
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	jd, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jd)

}

func GetWholesaleCookies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	client := f.NewFaunaClient(os.Getenv("FAUNA_TOKEN"))

	data, err := getFaunaMap(client, "Wholesale", true)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	jd, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jd)
}
