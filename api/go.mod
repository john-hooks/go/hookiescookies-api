module gitlab.com/hooksie1/hookiescookies-api/api

go 1.13

require (
	github.com/fauna/faunadb-go v2.0.0+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/stretchr/testify v1.5.1 // indirect
	gopkg.in/fauna/faunadb-go.v2 v2.0.0
)
