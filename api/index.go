package api

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	cookies "gitlab.com/hooksie1/hookiescookies-api/api/cookies"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "This is the api")
}

func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		log.Printf(
			"%s %s %s %s",
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
		)
	})
}

func newRouter(r Routes) *mux.Router {
	router := mux.NewRouter()

	for _, route := range r {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path("/api" + route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}

func getRoutes() Routes {
	routes := Routes{
		Route{
			"Index",
			"GET",
			"/",
			index,
		},
		Route{
			"GetCookies",
			"GET",
			"/cookies",
			cookies.GetCookies,
		},
		Route{
			"GetCookieById",
			"GET",
			"/cookie/{cookieId}",
			cookies.GetCookieById,
		},
		Route{
			"GetCookiesByIngredients",
			"GET",
			"/ingredients/{ingredient}",
			cookies.GetCookieByIngredients,
		},
		Route{
			"GetSiteCookies",
			"GET",
			"/site/cookies",
			cookies.GetSiteCookies,
		},
		Route{
			"GetWholesaleCookies",
			"GET",
			"/wholesale/cookies",
			cookies.GetWholesaleCookies,
		},
	}
	return routes
}

func Handler(w http.ResponseWriter, r *http.Request) {
	log.Println("handling request: ", r.URL.Path)

	routes := getRoutes()

	router := newRouter(routes)

	router.ServeHTTP(w, r)
}
